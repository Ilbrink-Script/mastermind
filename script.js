const COLORS = [1, 2, 3, 4, 5, 6];
const CODE_LENGTH = 4;
const BLANK_PEGS_ALLOWED = false;

let solution;

// display a message in the message paragraph
const setMessage = (msg, msgClass) => {
    document.getElementById('intro').classList.toggle('hide', true);
    
    const msgP = document.getElementById('msg');
    msgP.innerText = msg;
    msgP.setAttribute('class', msgClass);
    msgP.classList.toggle('hide', false);
};

// function to show and remove cover
const showCover = show => {
    document.getElementById('cover').classList.toggle('hide', !show);
};

// generate a solution  with random picked values from the colors array
const generateSolution = function () {
    const solution = [];
    for (let i = 0; i < CODE_LENGTH; i++) {
        solution.push(COLORS[Math.floor(Math.random() * COLORS.length)]);
    }

    // render solution
    const solutionCodeArr = document.querySelectorAll('#solution .peg');
    for (let i = 0; i < solution.length; i++) {
        insertColorPeg(solutionCodeArr[i], solution[i]);
    }

    return solution;
};

// inserts a color peg in the hole
const insertColorPeg = (el, colorNum) => {
    const colorPeg = document.createElement('div');
    colorPeg.setAttribute('class', `color-${colorNum}`);
    el.innerHTML = '';
    el.appendChild(colorPeg);
};

// adds draggable pens to put in the holes
const addDraggablePegs = () => {
    for (const el of document.querySelectorAll('#pegbox .peg div')) {
        // the color number extracted from the class name
        const colorNum = el.getAttribute('class').split('-')[1];

        // make dragable
        el.draggable = true;
        el.ondragstart = event => {
            event.dataTransfer.setData('text', colorNum);
        };

        // double click
        el.addEventListener('dblclick', event => {
            const emptyCodePeg = document.querySelector('.active.row .code .peg:empty');
            if (emptyCodePeg) {
                insertColorPeg(emptyCodePeg, colorNum);
            }
        });
    };
};

// adds dropable functionality
const addDroppablePegs = () => {
    for (const el of document.querySelectorAll('#guesses .code .peg')) {
        el.ondragover = event => {
            // only allow drops in the active row
            if (el.parentElement.parentElement.classList.contains('active')) {
                event.preventDefault();
            }
        };
        el.ondrop = event => {
            // only allow drops in the active row
            if (el.parentElement.parentElement.classList.contains('active')) {
                const colorNum = event.dataTransfer.getData('text');

                if (colorNum > 0) {
                    event.preventDefault();
                    insertColorPeg(el, colorNum);
                } else {
                    // it appears from random elements on clicking twice /r/n is set as DragEvent data
                    console.log('invalid \\r\\n DragEvent happened?', colorNum === '\r\n', event);
                }
            }
        };
    }
};

// builds an object to keep track of the matches for each posible color
const buildColorMatchesObj = () => {
    const matches = {};
    for (let i = 0; i < COLORS.length; i++) {
        matches[COLORS[i]] = {
            position: 0,
            solutionCount: 0,
            guessCount: 0,
            get color() {
                let color = 0;
                if (this.solutionCount > this.position && this.guessCount > this.position) {
                    color = Math.min(this.solutionCount, this.guessCount) - this.position;
                }
                return color;
            }
        }
    }
    return matches;
};

// fills and returns a colorMatches object with the data of the solution and provided guess
const getColorMatches = (guess) => {
    const matches = buildColorMatchesObj();
    for (let i = 0; i < solution.length; i++) {
        const solutionColor = solution[i];
        const guessColor = guess[i];

        if (matches[solutionColor]) {
            matches[solutionColor].solutionCount++;
        }
        if (matches[guessColor]) {
            matches[guessColor].guessCount++;
        }

        if (solutionColor === guessColor) {
            matches[solutionColor].position++;
        }
    }
    return matches;
};

// return the postion and color key peg data for this guess
const getKeysForGuess = (guess) => {
    // get match data
    const matches = getColorMatches(guess);

    // result object
    const keys = {
        position: 0,
        color: 0
    };

    // loop through each color and mutate any position or color pegs
    for (const match of Object.values(matches)) {
        keys.position += match.position;
        keys.color += match.color;
    };

    return keys;
};

// returns an array with the color numbers (1-6 or 0) guessed in the row
const getGuessColors = row => {
    const guessColors = [];
    for (const peg of row.querySelectorAll('.code .peg')) {
        let colorNum = 0; // no color / transparent
        if (peg.childElementCount > 0) {
            colorNum = parseInt(peg.firstElementChild.className.split('-')[1]);
        }
        guessColors.push(colorNum);
    }
    return guessColors;
};

// checks row and updates the key pegs
const checkRow = (row) => {
    const guessColors = getGuessColors(row);

    // check for transparent pegs
    if (!BLANK_PEGS_ALLOWED && guessColors.indexOf(0) >= 0) {
        return false;
    }

    // the row will be processed so we can deactiveate the row
    row.classList.toggle('active');

    // update key pegs
    const keys = getKeysForGuess(guessColors);
    const keyPegs = row.querySelectorAll('.keys .peg');
    let keyPegIdx = 0;
    for (let i = 0; i < keys.position; i++) {
        keyPegs[keyPegIdx].classList.toggle('position', true);
        keyPegIdx++;
    }
    for (let i = 0; i < keys.color; i++) {
        keyPegs[keyPegIdx].classList.toggle('color', true);
        keyPegIdx++;
    }

    return keys;
};

// toggles the disabled attribute on all buttons
const setupButtons = (gameFinished) => {
    document.getElementById('check').toggleAttribute('disabled', gameFinished);
    document.getElementById('reset').toggleAttribute('disabled', !gameFinished);
};

// reveal the solution and show a corresponding message
const endGame = keys => {
    // reveal solution
    showCover(false);

    // display approriate message
    if (keys.position === CODE_LENGTH) {
        setMessage('Congratulations, you truly are a Mastermind');
    } else {
        setMessage(`Better luck next time.\nYou had ${keys.position} colors in the correct position.`);
    }

    // update button to game finished status
    setupButtons(true);
};

// handles the various buttons
const addButtonHandlers = () => {
    // check button
    const checkBtn = document.getElementById('check');
    checkBtn.addEventListener('click', event => {
        const activeRow = document.querySelector('.active.row');

        // a valid guess has been processed
        const keys = checkRow(activeRow);
        // the row is invalid (contained transparent pegs)
        if (!keys) {
            setMessage('Please fill the whole row with colored code pegs.', 'error');
        } 
        // you've decyphered the code
        else if (keys.position === CODE_LENGTH) {
            endGame(keys);
        }
        // the row was processed, so proceed to the next row
        else {
            // retrieves next row in game (but previous element in the DOM)
            const nextRow = activeRow.previousElementSibling;
            if (nextRow) {
                nextRow.classList.toggle('active');
                setMessage('Please continue.');
            } else {
                // game over
                setupButtons(true);
            }
        }
    });

    // reset button
    const resetBtn = document.getElementById('reset');
    resetBtn.addEventListener('click', event => {
        resetGame();
    });
};

// clears pegs fom row
const resetRow = (row) => {
    // remove key pegs
    for (const peg of row.querySelectorAll('.keys .peg')) {
        peg.classList.toggle('position', false);
        peg.classList.toggle('color', false);
    }

    // remove score pegs
    for (const peg of row.querySelectorAll('.code .peg')) {
        peg.innerHTML = '';
    }
    return row;
};

// clears pegs from all guess rows
const resetGame = () => {
    let row;
    for (row of document.querySelectorAll('#guesses .row')) {
        resetRow(row);
    }

    // make the bottom row active again
    row.classList.toggle('active', true);

    // reset text paragraphs
    // document.getElementById('intro').classList.toggle('hide', false); // show intro text again?
    setMessage('Did you have fun?');

    // put cover back in place
    showCover(true);

    // reset buttons
    setupButtons(false);

    // reset the board
    initializeGame();
};

const initializeGame = () => {
    // setup listeners
    if (!solution) {
        addDraggablePegs();
        addDroppablePegs();
        addButtonHandlers();

        setMessage('Welcome!');
    }

    // generate and render a solution
    solution = generateSolution();
};

initializeGame();
